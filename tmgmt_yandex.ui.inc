<?php

/**
 * @file
 * Provides Yandex Translator ui controller.
 */

/**
 * Yandex translator ui controller.
 */
class TMGMTYandexTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Yandex API key'),
      '#default_value' => $translator->getSetting('api_key'),
      '#description' => t('Please enter your Yandex API key'),
    );

    return parent::pluginSettingsForm($form, $form_state, $translator);
  }

}
